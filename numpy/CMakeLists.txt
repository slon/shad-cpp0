execute_process(
  COMMAND "python3" -c "import numpy; print(numpy.get_include(), end='')"
  OUTPUT_VARIABLE NUMPY_INCLUDE_DIRS
  RESULT_VARIABLE NUMPY_NOTFOUND)

if (NUMPY_NOTFOUND)
  message(STATUS "Numpy not found")
else()
  add_shad_python_module(quantize SOLUTION_SRCS quantize.cpp)

  target_include_directories(quantize PRIVATE ${NUMPY_INCLUDE_DIRS})
endif()
